<?php
declare(strict_types=1);

namespace BehatLocalCodeCoverage;

use Behat\Behat\EventDispatcher\Event\AfterFeatureTested;
use Behat\Behat\EventDispatcher\Event\FeatureTested;
use Behat\Behat\Hook\Scope\AfterScenarioScope;
use Behat\Behat\Hook\Scope\BeforeScenarioScope;
use Behat\Hook\AfterScenario;
use Behat\Hook\BeforeScenario;
use Behat\Testwork\EventDispatcher\Event\AfterSuiteTested;
use Behat\Testwork\EventDispatcher\Event\SuiteTested;
use LiveCodeCoverage\CodeCoverageFactory;
use LiveCodeCoverage\Storage;
use SebastianBergmann\CodeCoverage\CodeCoverage;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

final class LocalCodeCoverageListener implements EventSubscriberInterface
{
    private ?CodeCoverage $coverage;

    public function __construct(
        private readonly string $phpunitXmlPath,
        private readonly string $targetDirectory,
        private readonly string $splitBy,
        private readonly bool $coverageEnabled,
    ) {
    }

    public static function getSubscribedEvents(): array
    {
        return [
            SuiteTested::BEFORE => 'beforeSuite',
            FeatureTested::AFTER => 'afterFeature',
            SuiteTested::AFTER => 'afterSuite'
        ];
    }

    public function beforeSuite(SuiteTested $event): void
    {
        if (!$this->coverageEnabled) {
            return;
        }

        try {
            $this->coverage = CodeCoverageFactory::createFromPhpUnitConfiguration($this->phpunitXmlPath);
        } catch (\RuntimeException $ex) {
            echo PHP_EOL . $ex->getMessage() . PHP_EOL;
        }
    }

    /**
     * We use behat hook instead of behat event because BeforeScenario hook
     * covers cases that are not handled by behat events.
     */
    #[BeforeScenario]
    public function beforeScenario(BeforeScenarioScope $event): void
    {
        if (!$this->coverageEnabled) {
            return;
        }

        $coverageId = $event->getFeature()->getFile() . ':' . $event->getScenario()->getLine();

        $this->coverage->start($coverageId);
    }

    /**
     * We use behat hook instead of behat event because BeforeScenario hook
     * covers cases that are not handled by behat events.
     */
    #[AfterScenario]
    public function afterScenario(AfterScenarioScope $event): void
    {
        if (!$this->coverageEnabled) {
            return;
        }

        $this->coverage->stop();
    }

    public function afterFeature(AfterFeatureTested $event): void
    {
        if (!$this->coverageEnabled || 'feature' !== $this->splitBy) {
            return;
        }

        $parts = pathinfo($event->getFeature()->getFile());
        Storage::storeCodeCoverage(
            $this->coverage,
            $this->targetDirectory,
            sprintf('%s-%s', basename($parts['dirname']), $parts['filename'])
        );
    }

    public function afterSuite(SuiteTested $event): void
    {
        // there could also be an AfterSuiteAborted event
        if (! $event instanceof AfterSuiteTested) {
            return;
        }

        if (!$this->coverageEnabled) {
            return;
        }

        if ('suite' === $this->splitBy) {
            Storage::storeCodeCoverage($this->coverage, $this->targetDirectory, $event->getSuite()->getName());
        }

        $this->reset();
    }

    private function reset(): void
    {
        $this->coverage = null;
    }
}
